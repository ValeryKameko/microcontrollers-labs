#include <util/delay.h>
#include <avr/io.h>
#include "dht11.h"


// TODO: fix interrupts

inline static uint8_t dht11_read_byte(const io_pin_t *pin) {
	uint8_t value = 0;
	for (uint8_t i = 0; i < 8; ++i) {
		value <<= 1;
		while (!io_pin_read(pin));
		
		_delay_us(50);
		if (io_pin_read(pin))
			value |= 1;
		
		while (io_pin_read(pin));
	}
	return value;
}


bool dht11_read_data(const dht11_t *dht11, dht11_data_t *data) {
	const io_pin_t *pin = &(dht11->pin);
	io_pin_set_out(pin);
	io_pin_write(pin, 0);
	_delay_ms(18);
	
	io_pin_write(pin, 1);
	io_pin_set_in(pin);
	_delay_us(80);
	
	if (io_pin_read(pin))
		return false;
	
	_delay_us(80);
	
	while (io_pin_read(pin));
	
	data->humidity_integral = dht11_read_byte(pin);
	data->humidity_decimal = dht11_read_byte(pin);
	data->temperature_integral = dht11_read_byte(pin);
	data->temperature_decimal = dht11_read_byte(pin);
	uint8_t parity = dht11_read_byte(pin);
	
	uint8_t sum =
		data->humidity_integral + 
		data->humidity_decimal +
		data->temperature_integral +
		data->temperature_decimal;
	return parity == sum;
}
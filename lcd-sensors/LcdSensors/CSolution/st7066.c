#include "st7066.h"
#include <util/delay.h>


enum {
	COMMAND_CLEAR_DISPLAY        = 0x001,
	COMMAND_RETURN_HOME          = 0x002,
	COMMAND_ENTRY_MODE_SET       = 0x004,
	COMMAND_DISPLAY_ON_OFF       = 0x008,
	COMMAND_CURSOR_DISPLAY_SHIFT = 0x010,
	COMMAND_FUNCTION_SET         = 0x020,
	COMMAND_SET_CGRAM_ADDRESS    = 0x040,
	COMMAND_SET_DDRAM_ADDRESS    = 0x080,
	COMMAND_READ_BF_AND_ADDRESS  = 0x100,
	COMMAND_WRITE_RAM            = 0x200,
	COMMAND_READ_RAM             = 0x300,
};

enum {
	COMMAND_BIT_SHIFT              = 0,
	COMMAND_BIT_INCREASE_DECREASE  = 1,
	COMMAND_BIT_CURSOR_POSITION_ON = 0,
	COMMAND_BIT_CURSOR_ON          = 1,
	COMMAND_BIT_DISPLAY_ON         = 2,
	COMMAND_BIT_RIGHT_LEFT         = 2,
	COMMAND_BIT_SHIFT_CONTROL      = 3,
	COMMAND_BIT_INTERFACE_DATA     = 4,
	COMMAND_BIT_NUMBER_OF_LINES    = 3,
	COMMAND_BIT_FONT_SIZE          = 2,
	COMMAND_BIT_BUSY_FLAG          = 7,
};

static inline void base_delay() {
	_delay_us(80);
}

static inline void secondary_delay() {
	_delay_ms(2);
}

static void st7066_transmit_byte_with_mode(const st7066_t *st7066, st7066_interfacing_mode_t mode, uint16_t command) {
	out_pin_write(&(st7066->rs_pin), (command & 0x200) != 0);
	out_pin_write(&(st7066->rw_pin), (command & 0x100) != 0);
	
	for (uint8_t i = 0; i < mode; i++) {
		io_pin_set_out(st7066->db_pins + i);
		io_pin_write(st7066->db_pins + i, (command & _BV(i)) != 0);
	}
		
	out_pin_write(&(st7066->en_pin), 1);
	_delay_us(1);
	out_pin_write(&(st7066->en_pin), 0);
}

static uint8_t st7066_receive_byte(const st7066_t *st7066, uint8_t control) {
	out_pin_write(&(st7066->rs_pin), control & 0x1);
	out_pin_write(&(st7066->rw_pin), control & 0x2);
	
	const uint8_t db_bins_count = st7066->interfacing_mode;
	uint8_t read_cycles = (st7066->interfacing_mode == ST7066_4BIT_MODE) ? 2 : 1;
	uint8_t byte = 0;
	
	for (uint8_t i = 0; i < db_bins_count; i++) {
		io_pin_set_in(st7066->db_pins + i);
		io_pin_write(st7066->db_pins + i, 0);
	}

	while (read_cycles-- > 0) {
		byte <<= 4;
		
		out_pin_write(&(st7066->en_pin), 1);
		_delay_us(1);
		out_pin_write(&(st7066->en_pin), 0);
		
		for (uint8_t i = 0; i < db_bins_count; i++) {
			if (io_pin_read(st7066->db_pins + i))
			byte |= _BV(i);
		}
	}
	return byte;
}

static void st7066_transmit_byte(const st7066_t *st7066, uint16_t command) {	
	if (st7066->interfacing_mode == ST7066_4BIT_MODE) {
		st7066_transmit_byte_with_mode(st7066, ST7066_4BIT_MODE, (command & 0xf00) | (command >> 4));
		st7066_transmit_byte_with_mode(st7066, ST7066_4BIT_MODE, command);
	} else {
		st7066_transmit_byte_with_mode(st7066, ST7066_8BIT_MODE, command);
	}
}

void st7066_init(const st7066_t *st7066, const st7066_config_t *config) {
	out_pin_set_out(&(st7066->en_pin));
	out_pin_set_out(&(st7066->rw_pin));
	out_pin_set_out(&(st7066->rs_pin));

	out_pin_write(&(st7066->en_pin), 0);
#ifndef DEBUG
	_delay_ms(40);
#endif
	
	uint16_t command;
	if (config->interfacing_mode == ST7066_4BIT_MODE) {
		command = (COMMAND_FUNCTION_SET | _BV(COMMAND_BIT_INTERFACE_DATA)) >> 4;

		st7066_transmit_byte_with_mode(st7066, config->interfacing_mode, command);
		base_delay();
	}

	st7066_function_set(st7066, config->interfacing_mode, config->lines_mode, config->font_size);
	
	st7066_function_set(st7066, config->interfacing_mode, config->lines_mode, config->font_size);
	
	st7066_display_on_off(st7066, config->enable_display, config->enable_cursor, config->enable_cursor_position);
	
	st7066_clear(st7066);
	
	st7066_entry_mode_set(st7066, config->decrement_increment, config->shift);
}

void st7066_clear(const st7066_t *st7066) {
	st7066_transmit_byte(st7066, COMMAND_CLEAR_DISPLAY);
	secondary_delay();
}

void st7066_return_home(const st7066_t *st7066) {
	st7066_transmit_byte(st7066, COMMAND_RETURN_HOME);
	secondary_delay();
}

void st7066_entry_mode_set(const st7066_t *st7066, bool decrement_increment, bool shift) {
	uint16_t command = COMMAND_ENTRY_MODE_SET |
		(decrement_increment ? _BV(COMMAND_BIT_INCREASE_DECREASE) : 0) |
		(shift ? _BV(COMMAND_BIT_SHIFT) : 0);
	st7066_transmit_byte(st7066, command);
	base_delay();
}

void st7066_display_on_off(const st7066_t *st7066, bool enable_display, bool enable_cursor, bool enable_cursor_position) {
	uint16_t command = COMMAND_DISPLAY_ON_OFF |
		(enable_display ? _BV(COMMAND_BIT_DISPLAY_ON) : 0) |
		(enable_cursor ? _BV(COMMAND_BIT_CURSOR_ON) : 0) |
		(enable_cursor_position ? _BV(COMMAND_BIT_CURSOR_POSITION_ON) : 0);
	st7066_transmit_byte(st7066, command);
	base_delay();
}

void st7066_shift(const st7066_t *st7066, st7066_shift_target_t shift_target, bool is_left) {
	uint16_t command = COMMAND_CURSOR_DISPLAY_SHIFT |
		((shift_target == ST7066_SHIFT_DISPLAY) ? _BV(COMMAND_BIT_SHIFT_CONTROL) : 0) |
		(is_left ? _BV(COMMAND_BIT_RIGHT_LEFT) : 0);
	st7066_transmit_byte(st7066, command);
	base_delay();
}

void st7066_function_set(const st7066_t *st7066, st7066_interfacing_mode_t interfacing_mode, st7066_lines_mode_t lines_mode, st7066_font_size_t font_size) {
	uint16_t command = COMMAND_FUNCTION_SET |
		((interfacing_mode == ST7066_8BIT_MODE) ? _BV(COMMAND_BIT_INTERFACE_DATA) : 0) |
		((lines_mode == ST7066_2LINE_MODE) ? _BV(COMMAND_BIT_NUMBER_OF_LINES) : 0) |
		((font_size == ST7066_FONT_5X11) ? _BV(COMMAND_BIT_FONT_SIZE) : 0);
	st7066_transmit_byte(st7066, command);
	base_delay();
}

void st7066_set_cgram_address(const st7066_t *st7066, uint8_t address) {
	st7066_transmit_byte(st7066, COMMAND_SET_CGRAM_ADDRESS | (address & 0x3f));
	base_delay();
}

void st7066_set_ddram_address(const st7066_t *st7066, uint8_t address) {
	st7066_transmit_byte(st7066, COMMAND_SET_DDRAM_ADDRESS | (address & 0x7f));
	base_delay();
}

uint8_t st7066_read_byte(const st7066_t *st7066) {
	uint8_t byte = 0xff;
	while (byte & _BV(COMMAND_BIT_BUSY_FLAG))
		byte = st7066_receive_byte(st7066, COMMAND_READ_RAM >> 8);
	base_delay();
	
	return byte;
}

void st7066_write_byte(const st7066_t *st7066, uint8_t byte) {
	st7066_transmit_byte(st7066, COMMAND_WRITE_RAM | byte);
	base_delay();
}

void st7066_write_char(const st7066_t *st7066, char byte) {
	st7066_write_byte(st7066, byte);
}

void st7066_write_string(const st7066_t *st7066, const char *bytes) {
	for (const char *it = bytes; (*it) != 0; ++it) {
		st7066_write_char(st7066, *it);
	}
}

void st7066_set_font_char(const st7066_t *st7066, uint8_t index, const st7066_font_char_t *font_char) {
	for (uint8_t i = 0; i < 8; i++) {
		st7066_set_cgram_address(st7066, (index << 3) | i);
		st7066_write_byte(st7066, font_char->rows[i]);
	}
}

void st7066_set_font(const st7066_t *st7066, const st7066_font_t *font) {
	for (uint8_t i = 0; i < 8; i++) {
		st7066_set_font_char(st7066, i, font->chars + i);
	}
}
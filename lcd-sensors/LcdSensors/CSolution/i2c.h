#ifndef I2C_H_
#define I2C_H_

#include <stdint.h>
#include <stdbool.h>

typedef enum {
	I2C_PRESCALER_1  = 0b00,
	I2C_PRESCALER_4  = 0b01,
	I2C_PRESCALER_16 = 0b10,
	I2C_PRESCALER_64 = 0b11,
} i2c_prescaler_t;

typedef uint8_t i2c_address_t;

void i2c_config_master(i2c_prescaler_t prescaler, uint8_t bit_rate);

bool i2c_start(i2c_address_t address, bool is_read);
void i2c_stop();
bool i2c_send_byte(uint8_t byte);
bool i2c_send_bytes(uint8_t size, const uint8_t *bytes);
bool i2c_receive_byte(uint8_t *byte, bool ack);
bool i2c_receive_bytes(uint8_t size, uint8_t *bytes);

#endif /* I2C_H_ */
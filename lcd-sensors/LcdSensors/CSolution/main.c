#include <avr/io.h>
#include <avr/interrupt.h>
#include <stdbool.h>
#include <time.h>
#include <util/delay.h>
#include <stdio.h>
#include <math.h>
#include "config.h"

#include "st7066.h"
#include "ds3232.h"
#include "ds18b20.h"
#include "i2c.h"

volatile bool is_ds3232_interrupt = false;
volatile bool is_counter1_interrupt = false;

typedef enum {
	MENU_STATE_TIME = 0,
	MENU_STATE_TEMPERATURE,
	MENU_STATE_TEMPERATURE_HUMIDITY,
	MENU_STATE_MIN = MENU_STATE_TIME,
	MENU_STATE_MAX = MENU_STATE_TEMPERATURE_HUMIDITY,
} menu_state_t;

static inline void handle_ds3232_int() {
	if (!(PINC & _BV(DS3232_INT_BIT)))
		is_ds3232_interrupt = true;
}

ISR(PCINT1_vect) {
	if (PCIFR & _BV(DS3232_INT_PCIE))
		handle_ds3232_int();
}

ISR(TIMER1_COMPA_vect) {
	is_counter1_interrupt = true;
}

static void setup_pins() {
	in_pin_set_in(&SHIFT_LEFT_PIN);
	in_pin_set_in(&FUNC_PIN);
	in_pin_set_in(&SHIFT_RIGHT_PIN);
}

static void setup_st7066() {
	st7066_init(&ST7066, &ST7066_START_CONFIG);
	st7066_set_font_char(&ST7066, '\x01', CUSTOM_FONT + 0);
}

static void setup_ds3232() {
	// Setup time
	ds3232_write_time(&DS3232_INITIAL_TIME);
		
	// Disable 32khz
	ds3232_config_32khz(false, false);
		
	// Setup alarm each second
	ds3232_swap_alarm_flag(DS3232_ALARM1, (bool*)&is_ds3232_interrupt);
	ds3232_set_alarm1(DS3232_ALARM1_PER_SECOND, 0, 0, 0, 0);
	ds3232_set_alarm_interrupt(DS3232_ALARM1, true);
	is_ds3232_interrupt = false;
	
	//// Enable interrupt	
	*DS3232_INT_DDR &= ~_BV(DS3232_INT_BIT);
	*DS3232_INT_PCMSK |= _BV(DS3232_INT_BIT);
	PCICR |= _BV(DS3232_INT_PCIE);
}

static void setup_ds18b20() {
	const one_wire_address_t address = {
		.bytes = {0},
	};
	uint8_t last_choice_bit_index = 0;
	one_wire_search_rom(&ONE_WIRE_BUS, &address, &ds18b20_address1, &last_choice_bit_index);
	one_wire_search_rom(&ONE_WIRE_BUS, &ds18b20_address1, &ds18b20_address2, &last_choice_bit_index);
	
	ds18b20_write_scratchpad(&ds18b20_driver, &ds18b20_address1, &ds18b20_scratchpad1);
	ds18b20_write_scratchpad(&ds18b20_driver, &ds18b20_address2, &ds18b20_scratchpad2);

	ds18b20_start_conversion(&ds18b20_driver, NULL);
}

static void setup_counter1(uint8_t prescaler, uint16_t value) {
	// Enable CTC mode for counter 1
	cli();
	TCCR1A = 0;
	TCCR1B = (TCCR0B & ~(_BV(CS12) | _BV(CS11) | _BV(CS10))) | prescaler;
	TCCR1B |= _BV(WGM12);
	TCNT1  = 0;
	OCR1A  = value;
	TIMSK1 |= _BV(OCIE1A);
	sei();
}

static void disable_counter1() {
	TCCR1B = 0;
	TIMSK1 &= ~_BV(OCIE1A);
}

static void handle_shifting() {
	static bool is_shifting = false;
	
	bool prev_shifting_buttons = !in_pin_read(&SHIFT_RIGHT_PIN) || !in_pin_read(&SHIFT_LEFT_PIN);
	_delay_ms(5);
	bool shifting_buttons = !in_pin_read(&SHIFT_RIGHT_PIN) || !in_pin_read(&SHIFT_LEFT_PIN);
	if (prev_shifting_buttons == shifting_buttons) {
		if (is_shifting && !shifting_buttons) {
			is_shifting = false;
			disable_counter1();
		} else if (!is_shifting && shifting_buttons) {
			is_shifting = true;
					
			// Set Counter1 with 1024 prescaler
			setup_counter1(_BV(CS02) |  _BV(CS00), SCREEN_SHIFT_SPEED);
			// Force shift
			is_counter1_interrupt = true;
		}
	}
			
	if (is_counter1_interrupt) {
		is_counter1_interrupt = false;
				
		bool shift_right = !in_pin_read(&SHIFT_RIGHT_PIN);
		bool shift_left  = !in_pin_read(&SHIFT_LEFT_PIN);
		if (shift_right != shift_left) {
			st7066_shift(&ST7066, ST7066_SHIFT_DISPLAY, shift_left);
		}
	}
}

static bool handle_menu_switch(menu_state_t *menu_state) {
	static bool is_switched = false;
	
	if (in_pin_read(&FUNC_PIN)) {
		is_switched = false;
		return false;
	}
	_delay_ms(5);
	if (in_pin_read(&FUNC_PIN)) {
		is_switched = false;
		return false;
	}
	if (is_switched)
		return false;

	is_switched = true;
	st7066_clear(&ST7066);
	if (*menu_state == MENU_STATE_MAX)
		*menu_state = MENU_STATE_MIN;
	else
		(*menu_state)++;
	return true;
}

static float convert_fahrenheit(float celcius) {
	return celcius * 5.0 / 9.0 + 32;
}

static void refresh_info(menu_state_t state) {
	static char string_buffer[STRING_BUFFER_SIZE];
	switch (state) {
		case MENU_STATE_TIME:
		{
			const static char DATE_STRING_FORMAT[] = "%b/%d/%Y";
			const static char TIME_STRING_FORMAT[] = "%I(%p).%M.%S";
			static ds3232_time_t ds3232_time;
			static struct tm time;

			if (!ds3232_read_time(&ds3232_time))
				return;
			ds3232_convert_to_time(&ds3232_time, &time);
			
			strftime(string_buffer, sizeof(string_buffer), DATE_STRING_FORMAT, &time);
			st7066_set_ddram_address(&ST7066, 0x00);
			st7066_write_string(&ST7066, string_buffer);
			
			strftime(string_buffer, sizeof(string_buffer), TIME_STRING_FORMAT, &time);
			st7066_set_ddram_address(&ST7066, 0x40);
			st7066_write_string(&ST7066, string_buffer);
			return;
		}

		case MENU_STATE_TEMPERATURE:
		{
			const static char FIRST_LINE_FORMAT[]  = "1: %+5.1f\x01""C %+5.1f\x01""F mx %+5.1f\x01""C av %+5.1f\x01""C";
			const static char SECOND_LINE_FORMAT[] = "2: %+5.1f\x01""C %+5.1f\x01""F mn %+5.1f\x01""C";
			ds18b20_fetch_state(&ds18b20_driver);
			
			if (!ds18b20_read_scratchpad(&ds18b20_driver, &ds18b20_address1, &ds18b20_scratchpad1))
				return;
			if (!ds18b20_read_scratchpad(&ds18b20_driver, &ds18b20_address2, &ds18b20_scratchpad2))
				return;
			
			float temperature1 = (float)ds18b20_scratchpad1.temperature / (float)(1 << DS18B20_FRACTION_BITS);
			float temperature1_f = convert_fahrenheit(temperature1);
			
			float temperature2 = (float)ds18b20_scratchpad2.temperature / (float)(1 << DS18B20_FRACTION_BITS);
			float temperature2_f = convert_fahrenheit(temperature2);
			
			float temperature_min = fminf(temperature1, temperature2);
			float temperature_avg = (temperature1 + temperature2) / 2;
			float temperature_max = fmaxf(temperature1, temperature2);
			
			sprintf(string_buffer, FIRST_LINE_FORMAT, temperature1, temperature1_f, temperature_max, temperature_avg);
			st7066_set_ddram_address(&ST7066, 0x00);
			st7066_write_string(&ST7066, string_buffer);
			
			sprintf(string_buffer, SECOND_LINE_FORMAT, temperature2, temperature2_f, temperature_min);
			st7066_set_ddram_address(&ST7066, 0x40);
			st7066_write_string(&ST7066, string_buffer);
			
			if (!ds18b20_start_conversion(&ds18b20_driver, NULL))
				return;
			return;
		}
		
		case MENU_STATE_TEMPERATURE_HUMIDITY:
		{
			const static char FIRST_LINE_FORMAT[]  = "T %+3.0f\x01""C %+3.0f\x01""C mn %+3.0f\x01""C av %+3.0f\x01""C mx %+3.0f\x01""C";
			const static char SECOND_LINE_FORMAT[] = "H %3.0f%%  %3.0f%%  mn %3.0f%%  av %3.0f%%  mx %3.0f%%";

			static dht11_data_t dht11_data_1, dht11_data_2;

			if (!dht11_read_data(&DHT11_1, &dht11_data_1))
				return;
			
			if (!dht11_read_data(&DHT11_2, &dht11_data_2))
				return;

			float temperature1 = dht11_decode_temperature(&dht11_data_1);
			float humidity1 = dht11_decode_humidity(&dht11_data_1);
			
			float temperature2 = dht11_decode_temperature(&dht11_data_2);
			float humidity2 = dht11_decode_humidity(&dht11_data_2);
			
			float temperature_min = fminf(temperature1, temperature2);
			float temperature_avg = (temperature1 + temperature2) / 2;
			float temperature_max = fmaxf(temperature1, temperature2);
			
			
			float humidity_min = fminf(humidity1, humidity2);
			float humidity_avg = (humidity1 + humidity2) / 2;
			float humidity_max = fmaxf(humidity1, humidity2);
			
			sprintf(string_buffer, FIRST_LINE_FORMAT, temperature1, temperature2, temperature_min, temperature_avg, temperature_max);
			st7066_set_ddram_address(&ST7066, 0x00);
			st7066_write_string(&ST7066, string_buffer);
			
			sprintf(string_buffer, SECOND_LINE_FORMAT, humidity1, humidity2, humidity_min, humidity_avg, humidity_max);
			st7066_set_ddram_address(&ST7066, 0x40);
			st7066_write_string(&ST7066, string_buffer);
			
			return;
		}
	}
}

int main(void) {
	// Init LCD
	setup_st7066();

	// Init I2C to 100kHZ
	i2c_config_master(I2C_PRESCALER_1, 2);

	setup_ds3232();
	
	setup_ds18b20();
	
	setup_pins();
	sei();

	bool need_refresh = true;
	menu_state_t menu_state = MENU_STATE_MIN;
	
	while (true) {
		// Shift if need
		handle_shifting();
		
		// Change display menu
		if (handle_menu_switch(&menu_state)) {
			need_refresh = true;
		}
		
		// Refresh time
		if (need_refresh) {
			need_refresh = false;
			refresh_info(menu_state);
		}
				
		// Check DS3232
		if (is_ds3232_interrupt) {
			is_ds3232_interrupt = false;
			bool is_ds3232_alarm1 = false;
			if (ds3232_swap_alarm_flag(DS3232_ALARM1, &is_ds3232_alarm1) && is_ds3232_alarm1)
				need_refresh = true;
		}
	}
    return 0;
}


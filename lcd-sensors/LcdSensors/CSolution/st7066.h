#ifndef ST7066_H_
#define ST7066_H_

#include "io.h"

typedef enum {
	ST7066_4BIT_MODE = 4,
	ST7066_8BIT_MODE = 8,
} st7066_interfacing_mode_t;

typedef enum {
	ST7066_1LINE_MODE,
	ST7066_2LINE_MODE,
} st7066_lines_mode_t;

typedef enum {
	ST7066_SHIFT_DISPLAY,
	ST7066_SHIFT_CURSOR,
} st7066_shift_target_t;

typedef enum {
	ST7066_FONT_5X8,
	ST7066_FONT_5X11,
} st7066_font_size_t;

typedef struct {
	const st7066_interfacing_mode_t interfacing_mode;
	const st7066_lines_mode_t lines_mode;
	const st7066_font_size_t font_size;
	const bool enable_display, enable_cursor, enable_cursor_position;
	const bool decrement_increment, shift;
} st7066_config_t;

typedef struct {
	uint8_t rows[8];
} st7066_font_char_t;

typedef struct {
	st7066_font_char_t chars[8];
} st7066_font_t;

typedef struct {
	const out_pin_t en_pin, rw_pin, rs_pin;
	const io_pin_t *db_pins;
	const st7066_interfacing_mode_t interfacing_mode;
} st7066_t;

void st7066_init(const st7066_t *st7066, const st7066_config_t *config);
void st7066_clear(const st7066_t *st7066);
void st7066_return_home(const st7066_t *st7066);

void st7066_entry_mode_set(const st7066_t *st7066, bool decrement_increment, bool shift);
void st7066_display_on_off(const st7066_t *st7066, bool enable_display, bool enable_cursor, bool enable_cursor_position);
void st7066_shift(const st7066_t *st7066, st7066_shift_target_t shift_target, bool is_left);
void st7066_function_set(const st7066_t *st7066, st7066_interfacing_mode_t interfacing_mode, st7066_lines_mode_t lines_mode, st7066_font_size_t font_size);

void st7066_set_cgram_address(const st7066_t *st7066, uint8_t address);
void st7066_set_ddram_address(const st7066_t *st7066, uint8_t address);

uint8_t st7066_read_byte(const st7066_t *st7066);
void st7066_write_byte(const st7066_t *st7066, uint8_t byte);

void st7066_write_char(const st7066_t *st7066, char byte);
void st7066_write_string(const st7066_t *st7066, const char *bytes);

void st7066_set_font_char(const st7066_t *st7066, uint8_t index, const st7066_font_char_t *font_char);
void st7066_set_font(const st7066_t *st7066, const st7066_font_t *font);

#endif /* ST7066_H_ */
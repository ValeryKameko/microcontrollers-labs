#include "i2c.h"
#include <avr/io.h>
#include <util/twi.h>


void i2c_config_master(i2c_prescaler_t prescaler, uint8_t bit_rate) {
	TWSR = (TWSR & 0b11) | prescaler;
	TWBR = bit_rate;
}

bool i2c_start(i2c_address_t address, bool is_read) {
	// Send START
	TWCR = _BV(TWEN) | _BV(TWINT) | _BV(TWSTA);
	while (!(TWCR & _BV(TWINT)));
	if (TW_STATUS != TW_START && TW_STATUS != TW_REP_START)
		return false;
	
	// Send SLA+R/W
	TWDR = (address << 1) | is_read;
	TWCR = _BV(TWEN) | _BV(TWINT);
	while (!(TWCR & _BV(TWINT)));
	if (TW_STATUS != TW_MT_SLA_ACK && TW_STATUS != TW_MR_SLA_ACK)
		return false;
	return true;
}

void i2c_stop() {
	TWCR = _BV(TWEN) | _BV(TWINT) | _BV(TWSTO);
}

bool i2c_send_byte(uint8_t byte) {
	// Send byte
	TWDR = byte;
	TWCR = _BV(TWEN) | _BV(TWINT);
	while (!(TWCR & _BV(TWINT)));
	if (TW_STATUS != TW_MT_DATA_ACK)
		return false;
	return true;
}

bool i2c_receive_byte(uint8_t *byte, bool ack) {
	TWCR = _BV(TWEN) | _BV(TWINT) | (ack ? _BV(TWEA) : 0);

	while (!(TWCR & _BV(TWINT)));
	if (TW_STATUS != TW_MR_DATA_ACK && TW_STATUS != TW_MR_DATA_NACK)
		return false;
		
	*byte = TWDR;
	return true;
}

bool i2c_send_bytes(uint8_t size, const uint8_t *bytes) {
	while (size-- > 0) {
		if (!i2c_send_byte(*(bytes++)))
			return false;
	}
	return true;
}

bool i2c_receive_bytes(uint8_t size, uint8_t *bytes) {	
	while (size-- > 0) {
		if (!i2c_receive_byte(bytes++, size != 0))
			return false;
	}
	return true;
}
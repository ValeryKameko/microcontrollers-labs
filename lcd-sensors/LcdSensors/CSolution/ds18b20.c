#include "ds18b20.h"

enum {
	COMMAND_CONVERT_T = 0x44,
	COMMAND_READ_SCRATCHPAD = 0xbe,
	COMMAND_WRITE_SCRATCHPAD = 0x4e,
	COMMAND_COPY_SCRATCHPAD = 0x48,
	COMMAND_RECALL_E2 = 0xb8,
};

bool ds18b20_start_conversion(ds18b20_t *ds18b20, const one_wire_address_t *address) {
	if (ds18b20->state != DS18B20_STATE_IDLE)
		return false;
	if (!one_wire_select_rom(ds18b20->bus, address))
		return false;
	one_wire_send_byte(ds18b20->bus, COMMAND_CONVERT_T);
	ds18b20->state = DS18B20_STATE_CONVERSION;
	return true;
}

bool ds18b20_start_copy_scratchpad(ds18b20_t *ds18b20, const one_wire_address_t *address) {
	if (ds18b20->state != DS18B20_STATE_IDLE)
		return false;
	if (!one_wire_select_rom(ds18b20->bus, address))
		return false;
	one_wire_send_byte(ds18b20->bus, COMMAND_COPY_SCRATCHPAD);
	ds18b20->state = DS18B20_STATE_COPY_SCRATCHPAD;
	return true;
}

bool ds18b20_start_recall_e2(ds18b20_t *ds18b20, const one_wire_address_t *address) {
	if (ds18b20->state != DS18B20_STATE_IDLE)
		return false;
	if (!one_wire_select_rom(ds18b20->bus, address))
		return false;
	one_wire_send_byte(ds18b20->bus, COMMAND_RECALL_E2);
	ds18b20->state = DS18B20_STATE_RECALL_E2;
	return true;
}

bool ds18b20_read_scratchpad(ds18b20_t *ds18b20, const one_wire_address_t *address, ds18b20_scratchpad_t *scratchpad) {
	if (!one_wire_match_rom(ds18b20->bus, address))
		return false;
	one_wire_send_byte(ds18b20->bus, COMMAND_READ_SCRATCHPAD);
	
	uint8_t bytes[8];
	one_wire_receive_bytes(ds18b20->bus, sizeof(bytes), bytes);
	uint8_t crc = one_wire_receive_byte(ds18b20->bus);
	
	scratchpad->temperature = ((uint16_t)bytes[0]) | (((uint16_t)bytes[1]) << 8);
	scratchpad->alarm_temperature_high = bytes[2];
	scratchpad->alarm_temperature_low = bytes[3];
	scratchpad->resolution = (bytes[4] >> 5) & 0b11;
	
	return true;
}

bool ds18b20_write_scratchpad(ds18b20_t *ds18b20, const one_wire_address_t *address, const ds18b20_scratchpad_t *scratchpad) {
	if (!one_wire_match_rom(ds18b20->bus, address))
		return false;
	one_wire_send_byte(ds18b20->bus, COMMAND_WRITE_SCRATCHPAD);
	
	one_wire_send_byte(ds18b20->bus, scratchpad->alarm_temperature_high);
	one_wire_send_byte(ds18b20->bus, scratchpad->alarm_temperature_low);
	one_wire_send_byte(ds18b20->bus, (scratchpad->resolution << 5) | 0b11111);
	
	return true;
}

void ds18b20_fetch_state(ds18b20_t *ds18b20) {
	if (ds18b20->state == DS18B20_STATE_IDLE)
		return;

	io_pin_set_in(&(ds18b20->bus->pin));
	if (io_pin_read(&(ds18b20->bus->pin))) {
		ds18b20->state = DS18B20_STATE_IDLE;
	}
}


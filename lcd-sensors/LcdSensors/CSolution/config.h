#ifndef CONFIG_H_
#define CONFIG_H_

#include <avr/io.h>
#include <avr/interrupt.h>
#include <time.h>

#include "io.h"
#include "st7066.h"
#include "ds3232.h"
#include "ds18b20.h"
#include "dht11.h"


// ST7066 config
enum { ST7066_INTERFACING_MODE = ST7066_8BIT_MODE };
const io_pin_t ST7066_DB_PINS[8] = {
	{.ddr = &DDRD, .port = &PORTD, .pin = &PIND, .bit = PIND0},
	{.ddr = &DDRD, .port = &PORTD, .pin = &PIND, .bit = PIND1},
	{.ddr = &DDRD, .port = &PORTD, .pin = &PIND, .bit = PIND2},
	{.ddr = &DDRD, .port = &PORTD, .pin = &PIND, .bit = PIND3},
	{.ddr = &DDRD, .port = &PORTD, .pin = &PIND, .bit = PIND4},
	{.ddr = &DDRD, .port = &PORTD, .pin = &PIND, .bit = PIND5},
	{.ddr = &DDRD, .port = &PORTD, .pin = &PIND, .bit = PIND6},
	{.ddr = &DDRD, .port = &PORTD, .pin = &PIND, .bit = PIND7},
};

const st7066_t ST7066 = {
	.en_pin = {.ddr = &DDRC, .port = &PORTC, .bit = PORTC2},
	.rw_pin = {.ddr = &DDRC, .port = &PORTC, .bit = PORTC1},
	.rs_pin = {.ddr = &DDRC, .port = &PORTC, .bit = PORTC0},
	.db_pins = ST7066_DB_PINS,
	.interfacing_mode = ST7066_INTERFACING_MODE,
};

const st7066_config_t ST7066_START_CONFIG = {
	.interfacing_mode = ST7066_INTERFACING_MODE,
	.lines_mode = ST7066_2LINE_MODE,
	.font_size = ST7066_FONT_5X8,
	.enable_display = true,
	.enable_cursor = false,
	.enable_cursor_position = false,
	.decrement_increment = true,
	.shift = false,
};

const st7066_font_char_t CUSTOM_FONT[8] = {
	{.rows = {
		0b00100,
		0b01010,
		0b00100,
		0b00000,
		0b00000,
		0b00000,
		0b00000,
		0b00000,
	}},
};


// DS18B20 config
const one_wire_t ONE_WIRE_BUS = {
	.pin = {.ddr = &DDRB, .port = &PORTB, .pin = &PINB, .bit = PINB2},
};
ds18b20_t ds18b20_driver = {
	.bus = &ONE_WIRE_BUS,
	.state = DS18B20_STATE_IDLE,
};
ds18b20_scratchpad_t ds18b20_scratchpad1 = {
	.alarm_temperature_high = 0,
	.alarm_temperature_high = 80,
	.resolution = DS18B20_RESOLUTION_9,
	.temperature = 0,
};
ds18b20_scratchpad_t ds18b20_scratchpad2 = {
	.alarm_temperature_high = 0,
	.alarm_temperature_high = 80,
	.resolution = DS18B20_RESOLUTION_9,
	.temperature = 0,
};
one_wire_address_t ds18b20_address1, ds18b20_address2;


// DHT11 config
const dht11_t DHT11_1 = {
	.pin = {.ddr = &DDRB, .port = &PORTB, .pin = &PINB, .bit = PINB0},
};
const dht11_t DHT11_2 = {
	.pin = {.ddr = &DDRB, .port = &PORTB, .pin = &PINB, .bit = PINB1},
};


// DS3232 config
#define DS3232_INT_vect PCINT1_vect
volatile uint8_t *DS3232_INT_PCMSK = &PCMSK1;
volatile uint8_t *DS3232_INT_DDR   = &DDRC;
volatile uint8_t *DS3232_INT_PIN   = &PINC;
volatile uint8_t *DS3232_INT_PORT  = &PORTC;
enum {
	DS3232_INT_BIT  = PCINT11,
	DS3232_INT_PCIE = PCIE1,
};
const ds3232_time_t DS3232_INITIAL_TIME = {
	.seconds = 0,
	.minutes = 0,
	.hours   = 9,
	.year    = 1984,
	.day     = SATURDAY,
	.date    = 26,
	.month   = OCTOBER,
};

enum {
	STRING_BUFFER_SIZE = 0x40,
};


// Controls config
const in_pin_t SHIFT_LEFT_PIN  = {.ddr = &DDRB, .pin = &PINB, .bit = PORTB3};
const in_pin_t FUNC_PIN        = {.ddr = &DDRB, .pin = &PINB, .bit = PORTB4};
const in_pin_t SHIFT_RIGHT_PIN = {.ddr = &DDRB, .pin = &PINB, .bit = PORTB5};

enum {
	SCREEN_SHIFT_SPEED = 4096, // ~1 symbols/sec
};

#endif /* CONFIG_H_ */
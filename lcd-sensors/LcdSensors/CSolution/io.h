#ifndef IO_H_
#define IO_H_

#include <avr/io.h>
#include <stdint.h>
#include <stdbool.h>


typedef struct {
	volatile uint8_t *ddr;
	const volatile uint8_t *pin;
	const uint8_t bit;
} in_pin_t;

inline void in_pin_set_in(const in_pin_t *pin) {
	*(pin->ddr) &= ~_BV(pin->bit);
}

inline uint8_t in_pin_read(const in_pin_t *pin) {
	return *(pin->pin) & _BV(pin->bit);
}


typedef struct {
	volatile uint8_t *ddr;
	volatile uint8_t *port;
	const uint8_t bit;
} out_pin_t;

inline void out_pin_set_out(const out_pin_t *pin) {
	*(pin->ddr) |= _BV(pin->bit);
}

inline void out_pin_write(const out_pin_t *pin, bool bit) {
	if (bit)
		*(pin->port) |= _BV(pin->bit);
	else
		*(pin->port) &= ~_BV(pin->bit);
}


typedef struct {
	volatile uint8_t *ddr;
	volatile uint8_t *port;
	const volatile uint8_t *pin;
	const uint8_t bit;
} io_pin_t;

inline void io_pin_set_in(const io_pin_t *pin) {
	*(pin->ddr) &= ~_BV(pin->bit);
}

inline void io_pin_set_out(const io_pin_t *pin) {
	*(pin->ddr) |= _BV(pin->bit);
}

inline bool io_pin_read(const io_pin_t *pin) {
	return (*(pin->pin) & _BV(pin->bit)) != 0;
}

inline void io_pin_write(const io_pin_t *pin, bool bit) {
	if (bit)
		*(pin->port) |= _BV(pin->bit);
	else
		*(pin->port) &= ~_BV(pin->bit);
}


#endif /* IO_H_ */
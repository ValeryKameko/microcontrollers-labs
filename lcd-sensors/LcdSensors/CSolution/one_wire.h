#ifndef ONE_WIRE_H_
#define ONE_WIRE_H_

#include "io.h"
#include <stdbool.h>
#include <stddef.h>

typedef struct {
	const io_pin_t pin;
} one_wire_t;

typedef struct {
	uint8_t bytes[8];
} one_wire_address_t;

void one_wire_send_byte(const one_wire_t *one_wire, uint8_t byte);
uint8_t one_wire_receive_byte(const one_wire_t *one_wire);

bool one_wire_read_rom(const one_wire_t *one_wire, one_wire_address_t *address);
bool one_wire_match_rom(const one_wire_t *one_wire, const one_wire_address_t *address);
bool one_wire_skip_rom(const one_wire_t *one_wire);
bool one_wire_search_rom(const one_wire_t *one_wire, const one_wire_address_t *previous_address, one_wire_address_t *next_address, uint8_t *last_choice_bit_index);


static inline bool one_wire_select_rom(const one_wire_t *one_wire, const one_wire_address_t *address) {
	if (address == NULL)
		return one_wire_skip_rom(one_wire);
	else
		return one_wire_match_rom(one_wire, address);
}

static inline void one_wire_send_bytes(const one_wire_t *one_wire, uint8_t size, const uint8_t *bytes) {
	while (size-- > 0) {
		one_wire_send_byte(one_wire, *(bytes++));
	}
}

static inline void one_wire_receive_bytes(const one_wire_t *one_wire, uint8_t size, uint8_t *bytes) {
	while (size-- > 0) {
		*(bytes++) = one_wire_receive_byte(one_wire);
	}
}

#endif /* ONE_WIRE_H_ */
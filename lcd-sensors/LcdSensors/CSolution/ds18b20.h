#ifndef DS18B20_H_
#define DS18B20_H_

#include "one_wire.h"

enum {
	DS18B20_FRACTION_BITS = 4,
};

typedef enum {
	DS18B20_STATE_IDLE,
	DS18B20_STATE_CONVERSION,
	DS18B20_STATE_COPY_SCRATCHPAD,
	DS18B20_STATE_RECALL_E2,
} ds18b20_state_t;

typedef enum {
	DS18B20_RESOLUTION_9  = 0b00,
	DS18B20_RESOLUTION_10 = 0b01,
	DS18B20_RESOLUTION_11 = 0b10,
	DS18B20_RESOLUTION_12 = 0b11,
} ds18b20_resolution_t;

typedef struct {
	ds18b20_resolution_t resolution;
	int16_t temperature;
	union {
		struct {
			uint8_t alarm_temperature_low, alarm_temperature_high;
		};
		struct {
			uint8_t user_bytes[2];
		};
	};
} ds18b20_scratchpad_t;

typedef struct {
	const one_wire_t *bus;
	ds18b20_state_t state;
} ds18b20_t;

bool ds18b20_start_conversion(ds18b20_t *ds18b20, const one_wire_address_t *address);
bool ds18b20_start_copy_scratchpad(ds18b20_t *ds18b20, const one_wire_address_t *address);
bool ds18b20_start_recall_e2(ds18b20_t *ds18b20, const one_wire_address_t *address);

bool ds18b20_read_scratchpad(ds18b20_t *ds18b20, const one_wire_address_t *address, ds18b20_scratchpad_t *scratchpad);
bool ds18b20_write_scratchpad(ds18b20_t *ds18b20, const one_wire_address_t *address, const ds18b20_scratchpad_t *scratchpad);

void ds18b20_fetch_state(ds18b20_t *ds18b20);


#endif /* DS18B20_H_ */
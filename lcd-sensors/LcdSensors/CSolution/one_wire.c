#include "one_wire.h"
#include <util/delay.h>


enum {
	COMMAND_READ_ROM = 0x33,
	COMMAND_MATCH_ROM = 0x55,
	COMMAND_SEARCH_ROM = 0xf0,
	COMMAND_SKIP_ROM = 0xcc,
};

static inline void one_wire_send_bit(const io_pin_t *pin, bool bit) {
	io_pin_write(pin, 0);
	_delay_us(8);
	
	io_pin_write(pin, bit);
	_delay_us(60);
	
	io_pin_write(pin, 1);
	_delay_us(8);
}

static inline bool one_wire_receive_bit(const io_pin_t *pin) {
	io_pin_set_out(pin);
	io_pin_write(pin, 0);
	_delay_us(8);

	io_pin_set_in(pin);
	_delay_us(8);
	
	bool bit = io_pin_read(pin);
	_delay_us(50);
	return bit;
}

void one_wire_send_byte(const one_wire_t *one_wire, uint8_t byte) {
	io_pin_set_out(&(one_wire->pin));
	for (uint8_t i = 0; i < 8; i++) {
		one_wire_send_bit(&(one_wire->pin), byte & 1);
		byte >>= 1;
	}
}

uint8_t one_wire_receive_byte(const one_wire_t *one_wire) {
	uint8_t result = 0;
	for (uint8_t i = 0; i < 8; i++) {
		result >>= 1;
		if (one_wire_receive_bit(&(one_wire->pin)))
			result |= 0x80;
	}
	return result;
}

static inline bool one_wire_reset(const io_pin_t *pin) {
	io_pin_set_out(pin);
	io_pin_write(pin, 0);
	_delay_us(480);
	
	io_pin_set_in(pin);
	_delay_us(70);
	if (io_pin_read(pin))
		return false;
	
	_delay_us(410);
	return true;
}

bool one_wire_read_rom(const one_wire_t *one_wire, one_wire_address_t *address) {
	const io_pin_t *pin = &(one_wire->pin);
	bool ok = one_wire_reset(pin);
	if (!ok)
		return false;
	
	io_pin_set_out(pin);
	one_wire_send_byte(one_wire, COMMAND_READ_ROM);
	
	one_wire_receive_bytes(one_wire, sizeof(one_wire_address_t), address->bytes);
	return true;
}

bool one_wire_match_rom(const one_wire_t *one_wire, const one_wire_address_t *address) {
	const io_pin_t *pin = &(one_wire->pin);
	bool ok = one_wire_reset(pin);
	if (!ok)
		return false;
	
	one_wire_send_byte(one_wire, COMMAND_MATCH_ROM);
	
	one_wire_send_bytes(one_wire, sizeof(one_wire_address_t), address->bytes);
	return true;
}

bool one_wire_skip_rom(const one_wire_t *one_wire) {
	const io_pin_t *pin = &(one_wire->pin);
	bool ok = one_wire_reset(pin);
	if (!ok)
		return false;
	
	one_wire_send_byte(one_wire, COMMAND_SKIP_ROM);
	return true;
}

bool one_wire_search_rom(const one_wire_t *one_wire, const one_wire_address_t *previous_address, one_wire_address_t *next_address, uint8_t *last_choice_bit_index) {
	const io_pin_t *pin = &(one_wire->pin);
	bool ok = one_wire_reset(pin);
	if (!ok)
		return false;

	one_wire_send_byte(one_wire, COMMAND_SEARCH_ROM);

	uint8_t choice_bit_index = 0;
	uint8_t bit_index = 0;
	
	for (uint8_t i = 0; i < sizeof(one_wire_address_t); i++) {
		next_address->bytes[i] = previous_address->bytes[i];
	}
	
	for (uint8_t i = 0; i < sizeof(one_wire_address_t); i++) {
		for (uint8_t j = 0; j < 8; j++) {
			++bit_index;
			
			bool bit = one_wire_receive_bit(pin);
			bool comp_bit = one_wire_receive_bit(pin);
			bool chosen_bit = 0;
			
			if (bit & comp_bit) {
				*last_choice_bit_index = 0;
				return true;
			} else if (bit) {
				chosen_bit = 1;
			} else if (comp_bit) {
				chosen_bit = 0;
			} else {
				if (bit_index < (*last_choice_bit_index)) {
					chosen_bit = (previous_address->bytes[i] & _BV(j)) != 0;
				} else if (bit_index > (*last_choice_bit_index)) {
					chosen_bit = 0;
				} else if (bit_index == (*last_choice_bit_index)) {
					chosen_bit = 1;
				}
				
				if (!chosen_bit) {
					choice_bit_index = bit_index;
				}
			}
			
			if (chosen_bit)
				next_address->bytes[i] |= _BV(j);
			else
				next_address->bytes[i] &= ~_BV(j);
				
			io_pin_set_out(pin);
			one_wire_send_bit(pin, chosen_bit);
		}
	}
	*last_choice_bit_index = choice_bit_index;
	return true;
}
#ifndef DS3232_H_
#define DS3232_H_

#include <time.h>
#include "i2c.h"

enum {
	DS3232_FRACTION_BITS = 2
};

typedef struct {
	uint8_t seconds;
	uint8_t minutes;
	uint8_t hours;
	uint8_t day;
	uint8_t date;
	uint8_t month;
	uint16_t year;
} ds3232_time_t;

typedef enum {
	DS3232_ALARM1_PER_SECOND    = 0x0f,
	DS3232_ALARM1_UP_TO_SECONDS = 0x0e,
	DS3232_ALARM1_UP_TO_MINUTES = 0x0c,
	DS3232_ALARM1_UP_TO_HOURS   = 0x08,
	DS3232_ALARM1_UP_TO_DATE    = 0x00,
	DS3232_ALARM1_UP_TO_DAY     = 0x10,
} ds3232_alarm1_type_t;

typedef enum {
	DS3232_ALARM2_PER_MINUTE    = 0x07,
	DS3232_ALARM2_UP_TO_MINUTES = 0x06,
	DS3232_ALARM2_UP_TO_HOURS   = 0x08,
	DS3232_ALARM2_UP_TO_DATE    = 0x00,
	DS3232_ALARM2_UP_TO_DAY     = 0x10,
} ds3232_alarm2_type_t;

typedef enum {
	DS3232_ALARM1 = 0x01,
	DS3232_ALARM2 = 0x02,
} ds3232_alarm_t;

typedef enum {
	DS3232_RATE_1HZ     = 0x00,
	DS3232_RATE_2_10_HZ = 0x08,
	DS3232_RATE_2_12_HZ = 0x10,
	DS3232_RATE_2_13_HZ = 0x18,
} ds3232_rate_t;

typedef enum {
	DS3232_CONVERSION_RATE_64S  = 0x00,
	DS3232_CONVERSION_RATE_128S = 0x10,
	DS3232_CONVERSION_RATE_256S = 0x20,
	DS3232_CONVERSION_RATE_512S = 0x30,
} ds3232_conversion_rate_t;

bool ds3232_read_time(ds3232_time_t *time);
bool ds3232_write_time(const ds3232_time_t *time);

bool ds3232_set_alarm1(ds3232_alarm1_type_t type, uint8_t seconds, uint8_t minutes, uint8_t hours, uint8_t date_day);
bool ds3232_set_alarm2(ds3232_alarm2_type_t type, uint8_t minutes, uint8_t hours, uint8_t date_day);
bool ds3232_set_alarm_interrupt(ds3232_alarm_t alarm, bool enable);
bool ds3232_swap_alarm_flag(ds3232_alarm_t alarm, bool *enable);

bool ds3232_enable_square_wave(ds3232_rate_t rate, bool battery_backed);
bool ds3232_config_32khz(bool enable, bool battery_backed);
bool ds3232_set_temperature_conversion_rate(ds3232_conversion_rate_t rate);
bool ds3232_start_temperature_conversion();

bool ds3232_read_temperature(int16_t *temperature);
bool ds3232_read_aging_offset(int8_t *aging_offset);

bool ds3232_write_user_registers(uint8_t address, uint8_t size, const uint8_t *bytes);
bool ds3232_read_user_registers(uint8_t address, uint8_t size, uint8_t *bytes);

void ds3232_convert_to_time(const ds3232_time_t *ds3232_time, struct tm *time);

#endif /* DS3232_H_ */

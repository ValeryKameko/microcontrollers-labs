#include "ds3232.h"
#include <time.h>
#include <avr/io.h>

enum {
	DS3232_ADDRESS = 0b1101000,
};

enum {
	REGISTER_SECONDS         = 0x00,
	REGISTER_MINUTES         = 0x01,
	REGISTER_HOURS           = 0x02,
	REGISTER_DAY             = 0x03,
	REGISTER_DATE            = 0x04,
	REGISTER_MONTH_CENTURY   = 0x05,
	REGISTER_YEAR            = 0x06,
	REGISTER_ALARM1_SECONDS  = 0x07,
	REGISTER_ALARM1_MINUTES  = 0x08,
	REGISTER_ALARM1_HOURS    = 0x09,
	REGISTER_ALARM1_DAY_DATE = 0x0a,
	REGISTER_ALARM2_MINUTES  = 0x0b,
	REGISTER_ALARM2_HOURS    = 0x0c,
	REGISTER_ALARM2_DAY_DATE = 0x0d,
	REGISTER_CONTROL         = 0x0e,
	REGISTER_CONTROL_STATUS  = 0x0f,
	REGISTER_AGING_OFFSET    = 0x10,
	REGISTER_TEMPERATURE_MSB = 0x11,
	REGISTER_TEMPERATURE_LSB = 0x12,
	REGISTER_USER            = 0x14,
};

enum {
	HOURS_12_24 = 6,
	HOURS_AM_PM = 5,
	CENTURY     = 7,
	INTCN       = 2,
	BBSQW       = 6,
	BB32KHZ     = 6,
	EN32KHZ     = 3,
	CRATE0      = 4,
	CRATE1      = 5,
	CONV        = 5
};

enum { YEAR_OFFSET = 1900 };

static inline uint8_t bcd2bin(uint8_t bcd) {
	return (bcd >> 4) * 10 + (bcd & 0xf);
}

static inline uint8_t bin2bcd(uint8_t bin) {
	return ((bin / 10) << 4) | (bin % 10);
}

static bool ds3232_read_registers(uint8_t offset, uint8_t size, uint8_t *bytes) {
	// Select word address
	if (!i2c_start(DS3232_ADDRESS, 0))
		return false;
	if (!i2c_send_byte(offset))
		return false;

	// Read bytes
	if (!i2c_start(DS3232_ADDRESS, 1))
		return false;

	if (!i2c_receive_bytes(size, bytes))
		return false;
	i2c_stop();
	return true;
}

static bool ds3232_write_registers(uint8_t offset, uint8_t size, const uint8_t *bytes) {
	// Select word address
	if (!i2c_start(DS3232_ADDRESS, 0))
		return false;

	if (!i2c_send_byte(offset))
		return false;
	
	// Write bytes
	if (!i2c_send_bytes(size, bytes))
		return false;
	i2c_stop();
	return true;
}

bool ds3232_read_time(ds3232_time_t *time) {
	uint8_t bytes[7];
	if (!ds3232_read_registers(REGISTER_SECONDS, sizeof(bytes), bytes))
		return false;
	
	// Decode time
	time->seconds = bcd2bin(bytes[0]);
	time->minutes = bcd2bin(bytes[1]);
	if (bytes[2] & _BV(HOURS_12_24))
		time->hours = bcd2bin((bytes[2] & ~_BV(HOURS_12_24)) ^ _BV(HOURS_AM_PM));
	else
		time->hours = bcd2bin(bytes[2]);
	time->day = bytes[3];
	time->date = bcd2bin(bytes[4]);
	time->month = bcd2bin(bytes[5] & ~_BV(CENTURY));
	time->year = bcd2bin(bytes[6]) + YEAR_OFFSET + ((bytes[5] & _BV(CENTURY)) ? 100 : 0);
	return true;
}

bool ds3232_write_time(const ds3232_time_t *time) {
	bool century = false;
	uint16_t year = time->year - YEAR_OFFSET;
	if (year >= 100) {
		year -= 100;
		century = true;
	}
	
	// Encode time
	uint8_t bytes[7] = {
		 bin2bcd(time->seconds),
		 bin2bcd(time->minutes),
		 bin2bcd(time->hours), // Use 24 hours format
		 time->day,
		 bin2bcd(time->date),
		 bin2bcd(time->month) | (century ? _BV(CENTURY) : 0),
		 bin2bcd(year),
	};

	return ds3232_write_registers(REGISTER_SECONDS, sizeof(bytes), bytes);
}

bool ds3232_set_alarm1(ds3232_alarm1_type_t type, uint8_t seconds, uint8_t minutes, uint8_t hours, uint8_t date_day) {
	// Encode time
	uint8_t bytes[4] = {
		((type & _BV(0)) ? 0x80 : 0) | seconds,
		((type & _BV(1)) ? 0x80 : 0) | minutes,
		((type & _BV(2)) ? 0x80 : 0) | hours,
		((type & _BV(3)) ? 0x80 : 0) | ((type & _BV(4)) ? 0x40 : 0) | date_day,
	};
	
	return ds3232_write_registers(REGISTER_ALARM1_SECONDS, sizeof(bytes), bytes);
}

bool ds3232_set_alarm2(ds3232_alarm2_type_t type, uint8_t minutes, uint8_t hours, uint8_t date_day) {
	// Encode time
	uint8_t bytes[3] = {
		((type & _BV(0)) ? 0x80 : 0) | minutes,
		((type & _BV(1)) ? 0x80 : 0) | hours,
		((type & _BV(2)) ? 0x80 : 0) | ((type & _BV(4)) ? 0x40 : 0) | date_day,
	};

	return ds3232_write_registers(REGISTER_ALARM2_MINUTES, sizeof(bytes), bytes);
}

bool ds3232_set_alarm_interrupt(ds3232_alarm_t alarm, bool enable) {
	uint8_t control;
	if (!ds3232_read_registers(REGISTER_CONTROL, 1, &control))
		return false;
	if (enable) {
		control |= alarm | _BV(INTCN);
	} else
		control &= ~alarm;
	return ds3232_write_registers(REGISTER_CONTROL, 1, &control);
}

bool ds3232_swap_alarm_flag(ds3232_alarm_t alarm, bool *enable) {
	uint8_t control_status;
	if (!ds3232_read_registers(REGISTER_CONTROL_STATUS, 1, &control_status))
		return false;
	bool enabled = (control_status & alarm) != 0;
	if (*enable)
		control_status |= alarm;
	else
		control_status &= ~alarm;
	*enable = enabled;
	return ds3232_write_registers(REGISTER_CONTROL_STATUS, 1, &control_status);
}

bool ds3232_enable_square_wave(ds3232_rate_t rate, bool battery_backed) {
	uint8_t control;
	if (!ds3232_read_registers(REGISTER_CONTROL, 1, &control))
		return false;
	control = (control & ~(0x1c | _BV(BBSQW))) | rate;
	if (battery_backed)
		control |= _BV(BBSQW);
	return ds3232_write_registers(REGISTER_CONTROL, 1, &control);
}

bool ds3232_config_32khz(bool enable, bool battery_backed) {
	uint8_t control_status;
	if (!ds3232_read_registers(REGISTER_CONTROL_STATUS, 1, &control_status))
		return false;
	control_status &= ~(_BV(BB32KHZ) | _BV(EN32KHZ));
	if (enable)
		control_status |= _BV(EN32KHZ);
	if (battery_backed)
		control_status |= _BV(BB32KHZ);
	return ds3232_write_registers(REGISTER_CONTROL_STATUS, 1, &control_status);
}

bool ds3232_set_temperature_conversion_rate(ds3232_conversion_rate_t rate) {
	uint8_t control_status;
	if (!ds3232_read_registers(REGISTER_CONTROL_STATUS, 1, &control_status))
		return false;
	control_status &= ~(_BV(CRATE1) | _BV(CRATE0));
	control_status |= rate;
	return ds3232_write_registers(REGISTER_CONTROL_STATUS, 1, &control_status);
}

bool ds3232_start_temperature_conversion() {
	uint8_t control_status;
	if (!ds3232_read_registers(REGISTER_CONTROL_STATUS, 1, &control_status))
		return false;
	control_status |= _BV(CONV);
	return ds3232_write_registers(REGISTER_CONTROL_STATUS, 1, &control_status);
}

bool ds3232_read_temperature(int16_t *temperature) {
	uint8_t bytes[2];
	if (!ds3232_read_registers(REGISTER_TEMPERATURE_MSB, sizeof(bytes), bytes))
		return false;
	
	*temperature = (((int16_t)(int8_t)bytes[0]) << 2) | (bytes[1] >> 6);
	return true;
}

bool ds3232_read_aging_offset(int8_t *aging_offset) {
	return ds3232_read_registers(REGISTER_AGING_OFFSET, 1, (uint8_t*)aging_offset);
}

bool ds3232_write_user_registers(uint8_t address, uint8_t size, const uint8_t *bytes) {
	return ds3232_write_registers(REGISTER_USER + address, size, bytes);
}

bool ds3232_read_user_registers(uint8_t address, uint8_t size, uint8_t *bytes) {
	return ds3232_read_registers(REGISTER_USER + address, size, bytes);
}

void ds3232_convert_to_time(const ds3232_time_t *ds3232_time, struct tm *time) {
	*time = (struct tm){
        .tm_sec   = ds3232_time->seconds,
        .tm_min   = ds3232_time->minutes,
        .tm_hour  = ds3232_time->hours,
        .tm_mday  = ds3232_time->date,
        .tm_wday  = -1,
        .tm_mon   = ds3232_time->month,
        .tm_year  = 84,
        .tm_yday  = -1,
        .tm_isdst = -1,
	};
//	mktime(time);
}
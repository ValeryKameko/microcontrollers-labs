#ifndef DHT11_H_
#define DHT11_H_

#include <stdbool.h>

#include "io.h"


typedef struct {
	const io_pin_t pin;
} dht11_t;


typedef struct {
	uint8_t humidity_integral, humidity_decimal;
	uint8_t temperature_integral, temperature_decimal;
} dht11_data_t;

bool dht11_read_data(const dht11_t *dht11, dht11_data_t *data);

static inline float dht11_decode_temperature(const dht11_data_t *data) {
	uint16_t temperature_code = ((uint16_t)data->temperature_integral << 8) + (uint16_t)data->temperature_decimal;
	return (float)temperature_code / 256;
}

static inline float dht11_decode_humidity(const dht11_data_t *data) {
	uint16_t humidity_code = ((uint16_t)data->humidity_integral << 8) + (uint16_t)data->humidity_decimal;
	return (float)humidity_code / 256;
}


#endif /* DHT11_H_ */
#define F_CPU 8000000UL

#include <stdbool.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/sfr_defs.h>


typedef struct {
	uint8_t id;
	uint8_t leds;
} state_t;

const state_t INITIAL_STATE = {.id = 0, .leds = 0b00000000};

state_t next_state(const state_t* state) {
	uint8_t id = state->id;
	switch (id % 8) {
		case 0:
		case 7:
			return (state_t){.id = id + 1, .leds = state->leds ^ 0b00011000};
		case 1:
		case 6:
			return (state_t){.id = id + 1, .leds = state->leds ^ 0b00100100};
		case 2:
		case 5:
			return (state_t){.id = id + 1, .leds = state->leds ^ 0b01000010};
		case 3:
		case 4:
			return (state_t){.id = id + 1, .leds = state->leds ^ 0b10000001};
		default:
			return INITIAL_STATE;
	}
}

volatile state_t current_state;

inline bool enabled() {
	return PINC & _BV(PINC0);
}

inline void set_leds(uint8_t leds) {
	PORTD = leds;
}

ISR(TIMER1_COMPA_vect) {
	bool en = enabled();
	if (en)
		current_state = next_state((const state_t*)&current_state);
}

void init_timer1() {
	// Clear timer counter
	TCNT1 = 0;
	
	// Set CTC mode
	TCCR1B |= _BV(WGM12);
	 
	// Set 1024 prescaler
	TCCR1B |= _BV(CS12) | _BV(CS10);
	
	// Set overflow interrupt
	TIMSK1 |= _BV(OCIE1A);
	
	// Set compare value
	OCR1A = F_CPU / 1024;
	
	sei();
}

inline void init_leds() {
	DDRD = 0xff;
	current_state = INITIAL_STATE;
}
 
int main() {
	init_leds();
	init_timer1();	

	while (1) {
		if (enabled()) {
			set_leds(current_state.leds);	
		} else {
			current_state = INITIAL_STATE;
			set_leds(0);
		}
	}
	return 0;
}
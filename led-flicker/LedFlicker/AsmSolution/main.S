#include <avr/io.h>
#include <avr/sfr_defs.h>


F_CPU		=	8000000
TIMER_COUNT	=	F_CPU / 1024


.section .data

leds_value:
	.byte	1
state_id:
	.byte	1


.section .text

// Init state
init_state:
	ser		r18
	out		_SFR_IO_ADDR(DDRD),		r18
	sts		leds_value,	r1
	sts		state_id,	r1
	ret


// Set next state
set_next_state:
	lds		r23,	leds_value
	lds		r24,	state_id
	andi	r24,	7

	// Check 0 or 7
	cpi		r24,	0
	breq	1f
	cpi		r24,	7
	breq	1f

	// Check 1 or 6
	cpi		r24,	1
	breq	2f
	cpi		r24,	6
	breq	2f

	// Check 2 or 5
	cpi		r24,	2
	breq	3f
	cpi		r24,	5
	breq	3f

	// Check 3 or 4
	cpi		r24,	3
	breq	4f
	cpi		r24,	4
	breq	4f

1:
	ldi		r25,	0b00011000
	eor		r23,	r25
	rjmp	set_next_state_end
2:
	ldi		r25,	0b00100100
	eor		r23,	r25
	rjmp	set_next_state_end
3:
	ldi		r25,	0b01000010
	eor		r23,	r25
	rjmp	set_next_state_end
4:
	ldi		r25,	0b10000001
	eor		r23,	r25
	rjmp	set_next_state_end

set_next_state_end:
	inc		r24
	sts		leds_value,	r23
	sts		state_id,	r24
	ret


// Set leds value
set_leds:
	out		_SFR_IO_ADDR(PORTD),	r24
	ret


// Check device enabled
enabled:
	clr		r24
	sbic	_SFR_IO_ADDR(PINC),		0
	ser		r24
	ret


// Initialize Timer1 to 1 second
init_timer:
	// Clear Timer counter
	sts		TCNT1H,		r1
	sts		TCNT1L,		r1

	// Set Timer top value
	ldi		r18,		hi8(TIMER_COUNT)
	sts		OCR1AH,		r18
	ldi		r18,		lo8(TIMER_COUNT)
	sts		OCR1AL,		r18

	// Set Timer CTC mode and prescaler 1024
	ldi		r18,		_BV(WGM12) | _BV(CS12) | _BV(CS10)
	sts		TCCR1B,		r18

	// Enable overflow interrupt
	ldi		r18,		_BV(OCIE1A)
	sts		TIMSK1,		r18

	sei
	ret


.global		TIMER1_COMPA_vect
TIMER1_COMPA_vect:
	push	r23
	push	r24
	push	r25
	in		r23,	_SFR_IO_ADDR(SREG)
	push	r23

	rcall	enabled
	sbrc	r24,		0
	rcall	set_next_state

	pop		r23
	out		_SFR_IO_ADDR(SREG),	r23
	pop		r25
	pop		r24
	pop		r23
	reti


// Main function
.global		main
main:
	// Init state and timer
	rcall	init_state
	rcall	init_timer

	// Main loop
1:
	// If enabled then r18 to leds_value else 0
	rcall	enabled
	tst		r24
	brne	2f
	clr		r18
	sts		leds_value,	r18
	sts		state_id,	r18

2:
	// Set leds to leds_value
	lds		r24,		leds_value
	rcall	set_leds

	rjmp	1b
	ret

.end
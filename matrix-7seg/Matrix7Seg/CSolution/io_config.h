#ifndef IO_CONFIG_H_
#define IO_CONFIG_H_

#include "io.h"
#include "spi.h"
#include "max7219_driver.h"
#include "matrix_keybord_driver.h"


const spi_t SPI = {
	.not_enable_pin = { .ddr = &DDRB, .port = &PORTB, .bit = 2 }
};

const max7219_driver_t MAX7219_DRIVER = {
	.spi = &SPI,
};

const pin_out_t MATRIX_KEYBORD_ROW_PINS[] = {
	{ .ddr = &DDRD, .port = &PORTD, .bit = 0 },
	{ .ddr = &DDRD, .port = &PORTD, .bit = 1 },
	{ .ddr = &DDRD, .port = &PORTD, .bit = 2 },
	{ .ddr = &DDRD, .port = &PORTD, .bit = 3 },
};

const pin_in_t MATRIX_KEYBORD_COLUMN_PINS[] = {
	{ .ddr = &DDRD, .pin = &PIND, .bit = 4 },
	{ .ddr = &DDRD, .pin = &PIND, .bit = 5 },
	{ .ddr = &DDRD, .pin = &PIND, .bit = 6 },
};

const char MATRIX_KEYBORD_CHARS[] = {
	'1', '2', '3',
	'4', '5', '6',
	'7', '8', '9',
	'*', '0', '#',
};

const matrix_keybord_driver_t MATRIX_KEYBORD_DRIVER = {
	.ROWS_COUNT = 4, .COLUMNS_COUNT = 3,
	.row_pins = MATRIX_KEYBORD_ROW_PINS,
	.column_pins = MATRIX_KEYBORD_COLUMN_PINS,
	.chars = MATRIX_KEYBORD_CHARS,
};

#endif /* IO_CONFIG_H_ */
#ifndef IO_H_
#define IO_H_

#include <avr/io.h>

#define _BVu8(bit) (uint8_t)_BV(bit)

typedef struct {
	volatile uint8_t * const port;
	volatile uint8_t * const ddr;
	const uint8_t bit;
} pin_out_t;

typedef struct {
	volatile uint8_t * const ddr;
	const volatile uint8_t * const pin;
	const uint8_t bit;
} pin_in_t;

#endif /* IO_H_ */
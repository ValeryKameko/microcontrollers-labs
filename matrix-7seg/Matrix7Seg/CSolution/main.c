#include <avr/io.h>
#include "io.h"
#include "io_config.h"

#include "spi.h"
#include "max7219_driver.h"
#include "matrix_keybord_driver.h"

typedef enum {
	CODE_BCD_8421,
	CODE_BCD_5421,
	CODE_BCD_2421,
	CODE_GRAY,
	CODE_JOHNSON
} code_t;

static const code_t CODE = CODE_BCD_5421;

uint8_t encode_digit(uint8_t value, code_t code) {
	switch (code) {
		case CODE_GRAY:
			return value ^ (value >> 1);
		case CODE_JOHNSON:
			return (0x0f0 >> (value & 0x7)) & 0xf;
		case CODE_BCD_8421:
			return value;
		case CODE_BCD_5421:
			return (value < 5) ? value : (0x8 | (value - 5));
		case CODE_BCD_2421:
			return (value < 5) ? value : (0x8 | (value - 2));
	}
	return value;
}

int main(void) {
	spi_master_init(1);
	matrix_keybord_init(&MATRIX_KEYBORD_DRIVER);
	
	max7219_init(&MAX7219_DRIVER);
	max7219_set_shutdown_mode(&MAX7219_DRIVER, false);
	max7219_set_decode_mode(&MAX7219_DRIVER, DECODE_3_0);
	max7219_set_scan_limit(&MAX7219_DRIVER, 4);
	max7219_set_intensity(&MAX7219_DRIVER, 0x8);
	
	while (true) {
		while (!matrix_keybord_poll(&MATRIX_KEYBORD_DRIVER, 1000, true));
		
		char pressed_char = 0;
		
		if (matrix_keybord_read(&MATRIX_KEYBORD_DRIVER, &pressed_char, 1) > 0) {
			if ('0' <= pressed_char && pressed_char <= '9') {
				uint8_t digit = pressed_char - '0';
				uint8_t encoded_digit = encode_digit(digit, CODE);
				for (uint8_t i = 0; i < 4; i++) {
					max7219_set_char(&MAX7219_DRIVER, 4 - i - 1, CHAR_0 + ((encoded_digit >> i) & 1), false);
				}
			} else if (pressed_char == '*') {
				for (uint8_t i = 0; i < 4; i++) {
					max7219_set_char(&MAX7219_DRIVER, i, CHAR_BLANK, false);
				}
			} else if (pressed_char == '#') {
				max7219_set_char(&MAX7219_DRIVER, 0, CHAR_H, false);
				max7219_set_char(&MAX7219_DRIVER, 1, CHAR_E, false);
				max7219_set_char(&MAX7219_DRIVER, 2, CHAR_L, false);
				max7219_set_char(&MAX7219_DRIVER, 3, CHAR_P, false);
			}
		}
		
		while (!matrix_keybord_poll(&MATRIX_KEYBORD_DRIVER, 1000, false));
	}
	
	return 0;
}


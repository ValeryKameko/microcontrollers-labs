#ifndef MATRIX_KEYBORD_H_
#define MATRIX_KEYBORD_H_

#include <avr/io.h>
#include "io.h"

#include <stdbool.h>
#include <stdint.h>

enum {
	MATRIX_KEYBORD_DEBOUNCE_CHECKS = 1,
	MATRIX_KEYBORD_DELAY_MS = 2,
};

typedef struct {
	const uint8_t COLUMNS_COUNT, ROWS_COUNT;
	const pin_out_t * const row_pins;
	const pin_in_t * const column_pins;
	const char * chars;
} matrix_keybord_driver_t;

void matrix_keybord_init(const matrix_keybord_driver_t *driver);
bool matrix_keybord_poll(const matrix_keybord_driver_t *driver, uint16_t timeout, bool need_pressed);
uint8_t matrix_keybord_read(const matrix_keybord_driver_t *driver, char * chars, uint8_t max_count);

#endif /* MATRIX_KEYBORD_H_ */
#include "max7219_driver.h"
#include <stdint.h>

typedef enum {
	OPCODE_NOOP = 0x0,
	OPCODE_SET_CHAR = 0x1,
	OPCODE_SET_DECODE_MODE = 0x9,
	OPCODE_SET_INTENSITY = 0xa,
	OPCODE_SET_SCAN_LIMIT = 0xb,
	OPCODE_SET_SHUTDOWN_MODE = 0xc,
	OPCODE_SET_DISPLAY_TEST = 0xf,
} max7219_opcode_t;

static inline uint16_t combine_op(uint8_t opcode, uint8_t reg) {
	return (uint16_t)(opcode << 8) | (uint16_t)(reg);
}

enum {
	POINT_OFFSET = 7,
};

void max7219_init(const max7219_driver_t *driver) {
	const pin_out_t *pin = &(driver->spi->not_enable_pin);
	*(pin->ddr) |= _BV(pin->bit);
	*(pin->port) |= _BV(pin->bit);
}

void max7219_nop(const max7219_driver_t *driver) {
	spi_send_word_be(driver->spi, combine_op(OPCODE_NOOP, 0));
}

void max7219_set_char(const max7219_driver_t *driver, uint8_t index, max7219_char_t value, bool point) {
	const uint8_t encoded_value = ((uint8_t) value) | (((uint8_t) point) << POINT_OFFSET);
	const uint8_t opcode = OPCODE_SET_CHAR + index;
	
	spi_send_word_be(driver->spi, combine_op(opcode, encoded_value));
}

void max7219_set_decode_mode(const max7219_driver_t *driver, max7219_decode_mode_t decode_mode) {
	spi_send_word_be(driver->spi, combine_op(OPCODE_SET_DECODE_MODE, (uint8_t) decode_mode));
}

void max7219_set_display_test(const max7219_driver_t *driver, bool enabled) {
	spi_send_word_be(driver->spi, combine_op(OPCODE_SET_DISPLAY_TEST, (uint8_t) enabled));
}

void max7219_set_scan_limit(const max7219_driver_t *driver, uint8_t count) {
	spi_send_word_be(driver->spi, combine_op(OPCODE_SET_SCAN_LIMIT, (count - 1) & 0x7));
}

void max7219_set_intensity(const max7219_driver_t *driver, uint8_t intensity) {
	spi_send_word_be(driver->spi, combine_op(OPCODE_SET_INTENSITY, intensity & 0x0f));
}

void max7219_set_shutdown_mode(const max7219_driver_t *driver, bool enabled) {
	spi_send_word_be(driver->spi, combine_op(OPCODE_SET_SHUTDOWN_MODE, (uint8_t) !enabled));
}

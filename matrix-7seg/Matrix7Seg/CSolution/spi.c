#include "spi.h"
#include <stdbool.h>
#include <avr/io.h>

const pin_out_t SPI_MOSI = { .port = &PORTB, .ddr = &DDRB, .bit = 3 };
const pin_out_t SPI_SCK  = { .port = &PORTB, .ddr = &DDRB, .bit = 5 };

void spi_master_init(uint8_t log2_speed_multiplier) {
	*(SPI_MOSI.ddr) |= _BV(SPI_MOSI.bit);
	*(SPI_SCK.ddr) |= _BV(SPI_SCK.bit);
	
	SPCR |= _BV(SPE) | _BV(MSTR);
	
	if (((log2_speed_multiplier >> 0) & 1) == 0)
		SPSR &= ~_BV(SPI2X);
	else
		SPSR |= _BV(SPI2X);
	
	if (((log2_speed_multiplier >> 1) & 1) == 0)
		SPCR &= ~_BV(SPR1);
	else
		SPCR |= _BV(SPR1);
	
	if (((log2_speed_multiplier >> 2) & 1) == 0)
		SPCR &= ~_BV(SPR1);
	else
		SPCR |= _BV(SPR1);
}

static inline void _spi_enable_slave(const pin_out_t *pin) {
	*(pin->port) &= ~_BVu8(pin->bit);	
}

static inline void _spi_disable_slave(const pin_out_t *pin) {
	*(pin->port) |= _BVu8(pin->bit);
}

static inline void _spi_send_byte(uint8_t value) {
	SPDR = value;
#ifndef DEBUG
	while (!(SPSR & _BV(SPIF)));
#endif
}

void spi_send_byte(const spi_t *spi, uint8_t value) {
	_spi_enable_slave(&(spi->not_enable_pin));
	_spi_send_byte(value);
	_spi_disable_slave(&(spi->not_enable_pin));
}

void spi_send_word_le(const spi_t *spi, uint16_t value) {
	_spi_enable_slave(&(spi->not_enable_pin));

	_spi_send_byte((uint8_t)(value));
	_spi_send_byte((uint8_t)(value >> 8));
	
	_spi_disable_slave(&(spi->not_enable_pin));
}

void spi_send_word_be(const spi_t *spi, uint16_t value) {
	_spi_enable_slave(&(spi->not_enable_pin));
	
	_spi_send_byte((uint8_t)(value >> 8));
	_spi_send_byte((uint8_t)(value));
	
	_spi_disable_slave(&(spi->not_enable_pin));
}
#ifndef MAX7219_DRIVER_H_
#define MAX7219_DRIVER_H_

#include <stdbool.h>
#include "spi.h"

typedef enum {
	CHAR_0 = 0x0,
	CHAR_1 = 0x1,
	CHAR_2 = 0x2,
	CHAR_3 = 0x3,
	CHAR_4 = 0x4,
	CHAR_5 = 0x5,
	CHAR_6 = 0x6,
	CHAR_7 = 0x7,
	CHAR_8 = 0x8,
	CHAR_9 = 0x9,
	CHAR_DASH = 0xa,
	CHAR_E = 0xb,
	CHAR_H = 0xc,
	CHAR_L = 0xd,
	CHAR_P = 0xe,
	CHAR_BLANK = 0xf,
} max7219_char_t;

typedef enum {
	NO_DECODE = 0x0,
	DECODE_0 = 0x1,
	DECODE_3_0 = 0xf,
	DECODE_7_0 = 0xff,
} max7219_decode_mode_t;

typedef struct {
	const spi_t * const spi;
} max7219_driver_t;


void max7219_init(const max7219_driver_t *driver);

void max7219_nop(const max7219_driver_t *driver);
void max7219_set_char(const max7219_driver_t *driver, uint8_t index, max7219_char_t value, bool point);
void max7219_set_decode_mode(const max7219_driver_t *driver, max7219_decode_mode_t decode_mode);
void max7219_set_display_test(const max7219_driver_t *driver, bool enabled);
void max7219_set_scan_limit(const max7219_driver_t *driver, uint8_t count);
void max7219_set_intensity(const max7219_driver_t *driver, uint8_t intensity);
void max7219_set_shutdown_mode(const max7219_driver_t *driver, bool enabled);

#endif /* MAX7219_DRIVER_H_ */
#include <util/delay.h>
#include "matrix_keybord_driver.h"

void matrix_keybord_init(const matrix_keybord_driver_t *driver) {
	for (uint8_t row = 0; row < driver->ROWS_COUNT; ++row) {
		const pin_out_t *pin = driver->row_pins + row;
		
		*(pin->ddr) |= _BV(pin->bit);
	}
	
	for (uint8_t column = 0; column < driver->COLUMNS_COUNT; ++column) {
		const pin_in_t *pin = driver->column_pins + column;
		
		*(pin->ddr) &= ~_BV(pin->bit);
	}
}

bool matrix_keybord_poll(const matrix_keybord_driver_t *driver, uint16_t timeout, bool need_pressed) {
	for (uint8_t row = 0; row < driver->ROWS_COUNT; ++row) {
		const pin_out_t *pin = driver->row_pins + row;	
		*(pin->port) |= _BV(pin->bit);
	}
	_delay_ms(MATRIX_KEYBORD_DELAY_MS);
	
	uint8_t debounce_checks = MATRIX_KEYBORD_DEBOUNCE_CHECKS;
	while (debounce_checks > 0 && timeout > 0) {
		bool is_pressed = false;
		for (uint8_t column = 0; column < driver->COLUMNS_COUNT; ++column) {
			const pin_in_t *pin = driver->column_pins + column;		
			is_pressed |= (bool)(*(pin->pin) & _BV(pin->bit));
		}
		debounce_checks = (is_pressed == need_pressed) ? (debounce_checks - 1) : MATRIX_KEYBORD_DEBOUNCE_CHECKS;
		_delay_ms(1);
		timeout--;
	}
	
	for (uint8_t row = 0; row < driver->ROWS_COUNT; ++row) {
		const pin_out_t *pin = driver->row_pins + row;
		*(pin->port) &= ~_BV(pin->bit);
	}
	return debounce_checks == 0;
}

uint8_t matrix_keybord_read(const matrix_keybord_driver_t *driver, char *chars, uint8_t max_count) {
	char *chars_it = chars;
	
	for (uint8_t row = 0; row < driver->ROWS_COUNT; ++row) {
		const pin_out_t *row_pin = driver->row_pins + row;
		*(row_pin->port) |= _BV(row_pin->bit);
		
		_delay_ms(MATRIX_KEYBORD_DELAY_MS);
		
		for (uint8_t column = 0; column < driver->COLUMNS_COUNT; ++column) {
			const pin_in_t *column_pin = driver->column_pins + column;
			
			if (max_count > 0 && (*(column_pin->pin) & _BV(column_pin->bit)) != 0) {
				--max_count;
				*(chars_it++) = *(driver->chars + row * driver->COLUMNS_COUNT + column);
			}
		}
		
		*(row_pin->port) &= ~_BV(row_pin->bit);
	}
	return chars_it - chars;
}

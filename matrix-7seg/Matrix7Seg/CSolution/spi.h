#ifndef SPI_H_
#define SPI_H_

#include <avr/io.h>
#include "io.h"

typedef struct {
	const pin_out_t not_enable_pin;
} spi_t;

void spi_master_init(uint8_t log2_speed_multiplier);

void spi_send_byte(const spi_t *spi, uint8_t value);
void spi_send_word_le(const spi_t *spi, uint16_t value);
void spi_send_word_be(const spi_t *spi, uint16_t value);


#endif /* SPI_H_ */
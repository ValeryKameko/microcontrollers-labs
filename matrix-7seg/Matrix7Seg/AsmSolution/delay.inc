#ifndef DELAY_INC_
#define DELAY_INC_

.macro DELAY_1MS reg1, reg2
#ifndef DEBUG
    ldi		\reg1 ,	0xcf
    ldi		\reg2 ,	0x01
99:
	sbiw	\reg1 ,	0x0001
    brne	99b
#endif
.endm

.macro DELAY_1S reg1, reg2, reg3
#ifndef DEBUG
    ldi		\reg1 ,	41
    ldi		\reg2 ,	150
    ldi		\reg3 ,	128
99:
	dec		\reg3
    brne	99b
    dec		\reg2
    brne	99b
    dec		\reg1
    brne	99b
#endif
.endm

#endif /* DELAY_INC_ */
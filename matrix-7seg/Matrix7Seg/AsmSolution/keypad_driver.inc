#ifndef KEYPAD_DRIVER_INC_
#define KEYPAD_DRIVER_INC_

#include "io.inc"

.set	KEYPAD_DRIVER_SIZE,					8
.set	KEYPAD_DRIVER_ROWS_OFFSET,			0
.set	KEYPAD_DRIVER_COLUMNS_OFFSET,		1
.set	KEYPAD_DRIVER_ROW_PORTS_OFFSET,		2
.set	KEYPAD_DRIVER_COLUMN_PINS_OFFSET,	4
.set	KEYPAD_DRIVER_CHARS_OFFSET,			6


.macro KEYPAD_DRIVER label, rows, columns, row_ports, column_pins, chars
\label :
	.byte	\rows
	.byte	\columns
	.word	\row_ports
	.word	\column_pins
	.word	\chars
.endm


.extern keypad_init
.extern keypad_poll
.extern keypad_read_char


#endif /* MATRIX_KEYBORD_DRIVER_INC_ */
#ifndef IO_INC_
#define IO_INC_


.set	IO_IN_DDR_OFFSET,	0
.set	IO_IN_PIN_OFFSET,	1
.set	IO_IN_BIT_OFFSET,	2
.set	IO_IN_PIN_SIZE,		3

.macro IO_IN_PIN label, ddr, pin, bit
.ifdef	label
\label :
.endif
	.byte	\ddr, \pin, _BV( \bit )
.endm


.set	IO_OUT_DDR_OFFSET,	0
.set	IO_OUT_PORT_OFFSET,	1
.set	IO_OUT_BIT_OFFSET,	2
.set	IO_OUT_PORT_SIZE,		3

.macro IO_OUT_PORT label, ddr, port, bit
.ifdef	label
\label :
.endif
	.byte	\ddr, \port, _BV( \bit )
.endm


#endif /* IO_INC_ */
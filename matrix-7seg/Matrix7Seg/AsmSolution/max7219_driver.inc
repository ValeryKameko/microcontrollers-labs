#ifndef MAX7219_DRIVER_INC_
#define MAX7219_DRIVER_INC_

#include "spi.inc"

// Chars
.set	CHAR_0,		0x0
.set	CHAR_1,		0x1
.set	CHAR_2,		0x2
.set	CHAR_3,		0x3
.set	CHAR_4,		0x4
.set	CHAR_5,		0x5
.set	CHAR_6,		0x6
.set	CHAR_7,		0x7
.set	CHAR_8,		0x8
.set	CHAR_9,		0x9
.set	CHAR_DASH,	0xa
.set	CHAR_E,		0xb
.set	CHAR_H,		0xc
.set	CHAR_L,		0xd
.set	CHAR_P,		0xe
.set	CHAR_EMPTY,	0xf
.set	CHAR_DP,	0x80

// Decode modes
.set	NO_DECODE,		0x00
.set	DECODE_0,		0x01
.set	DECODE_3_0,		0x0f
.set	DECODE_7_0,		0xff


.set	MAX7219_DRIVER_SIZE,			SPI_DRIVER_SIZE
.set	MAX7219_DRIVER_SPI_OFFSET,		0

.macro MAX7219_DRIVER label, not_ss_ddr, not_ss_port, not_ss_bit
\label :
	SPI_DRIVER label=\label, not_ss_ddr=\not_ss_ddr, not_ss_port=\not_ss_port, not_ss_bit=\not_ss_bit
.endm


.extern max7219_init
.extern max7219_noop
.extern max7219_set_char
.extern max7219_set_decode_mode
.extern max7219_set_intensity
.extern max7219_set_scan_limit
.extern max7219_set_shutdown_mode
.extern max7219_set_display_test


#endif /* MAX7219_DRIVER_INC_ */
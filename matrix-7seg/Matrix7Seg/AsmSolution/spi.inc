#ifndef SPI_INC_
#define SPI_INC_

#include "io.inc"

.set	SPI_DRIVER_SIZE,			IO_OUT_PORT_SIZE
.set	SPI_DRIVER_NOT_SS_OFFSET,	0

.macro SPI_DRIVER label, not_ss_ddr, not_ss_port, not_ss_bit
	IO_OUT_PORT label=\label, ddr=\not_ss_ddr, port=\not_ss_port, bit=\not_ss_bit
.endm

.extern spi_master_init
.extern spi_send_byte
.extern spi_send_word_be
.extern spi_send_word_le

#endif /* SPI_INC_ */
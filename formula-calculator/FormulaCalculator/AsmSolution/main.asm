.ESEG
result:
	.DW		0
variable_values:
	.DW		10, 2, 4, 5
variable_values_end:
	.EQU	VARIABLES_COUNT	=	(variable_values_end - variable_values) / 2

expression_value:
	.DB		"x1 3 - x2 * x3 - x4 3 - /", 0
expression_value_end:
	.EQU	EXPRESSION_SIZE	=	expression_value_end - expression_value - 1


.DSEG
variables:	.BYTE	VARIABLES_COUNT * 2
expression: .BYTE	EXPRESSION_SIZE


.CSEG
; Entry point
start:
    rcall	main
forever_loop:
	rjmp	forever_loop

.include	"eeprom.asm"
.include	"expression.asm"

; Main function
main:
	; Load variables
	ldi		r24,	LOW(variable_values)
	ldi		r25,	HIGH(variable_values)
	ldi		r22,	LOW(variables)
	ldi		r23,	HIGH(variables)
	ldi		r20,	2 * VARIABLES_COUNT
	rcall	eeprom_read_bytes

	; Load expression
	ldi		r24,	LOW(expression_value)
	ldi		r25,	HIGH(expression_value)
	ldi		r22,	LOW(expression)
	ldi		r23,	HIGH(expression)
	ldi		r20,	EXPRESSION_SIZE
	rcall	eeprom_read_bytes

	; Calc expression
	ldi		r24,	LOW(expression)
	ldi		r25,	HIGH(expression)
	ldi		r22,	LOW(variables)
	ldi		r23,	HIGH(variables)
	rcall	calc_expression
	movw	ZH:ZL,	r25:r24

	; Save result
	ldi		r24,	LOW(result)
	ldi		r25,	HIGH(result)
	mov		r22,	ZL
	rcall	eeprom_write_byte

	ldi		r24,	LOW(result + 1)
	ldi		r25,	HIGH(result + 1)
	mov		r22,	ZH
	rcall	eeprom_write_byte
	ret
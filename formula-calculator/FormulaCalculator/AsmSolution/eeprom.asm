; Read from address r25:r24 into r24
eeprom_read_byte:
	; Wait last write completion
	sbic	EECR,	EEPE
	rjmp	eeprom_read_byte

	; Set read address r25:r24
	out		EEARL,	r24
	out		EEARH,	r25

	; Read into r24
	sbi		EECR,	EERE
	in		r24,	EEDR
	ret


; Load r20 bytes from EEPROM r25:r24 into SRAM r23:r22
eeprom_read_bytes:
	movw	XH:XL,	r25:r24
	movw	ZH:ZL,	r23:r22
	
	rjmp	eeprom_read_bytes_loop_end
eeprom_read_bytes_loop:
	movw	r25:r24,XH:XL
	rcall	eeprom_read_byte
	adiw	XH:XL,	1
	
	st		Z+,		r24
	dec		r20
eeprom_read_bytes_loop_end:
	tst		r20
	brne	eeprom_read_bytes_loop
	ret


; Write r22 into address r25:r24
eeprom_write_byte:
	; Wait last write completion
	sbic	EECR,	EEPE
	rjmp	eeprom_write_byte

	; Set write address r25:r24
	out		EEARL,	r24
	out		EEARH,	r25

	; Write r22
	out		EEDR,	r22
	sbi		EECR,	EEMPE
	sbi		EECR,	EEPE
	ret

.CSEG

; Calc expression at r25:r24 with variables at r23:r22 return r25:r24
calc_expression:
	movw	XH:XL,	r25:r24

calc_expression_loop:
	ld		r30,	X+
	cpi		r30,	' '
	breq	calc_expression_loop

	tst		r30
	brne	jmp1
	rjmp	calc_expression_loop_end
jmp1:

	cpi		r30,	'x'
	brne	jmp2
	rjmp	calc_expression_handle_variable
jmp2:

	cpi		r30,	'+'
	brne	jmp3
	rjmp	calc_expression_handle_sum
jmp3:

	cpi		r30,	'-'
	brne	jmp4
	rjmp	calc_expression_handle_subtract
jmp4:

	cpi		r30,	'*'
	brne	jmp5
	rjmp	calc_expression_handle_multiply
jmp5:

	cpi		r30,	'/'
	brne	jmp6
	rjmp	calc_expression_handle_divide
jmp6:

	cpi		r30,	'0'
	brlo	calc_expression_loop_end
	cpi		r30,	'9' + 1
	brsh	calc_expression_loop_end
	rjmp	calc_expression_handle_immediate

calc_expression_loop_end:
	pop		r24
	pop		r25
	ret

; Handle table
calc_expression_handle_variable:
	; Load variable index into r30
	ld		r30,	X+
	subi	r30,	'1'
	lsl		r30

	; Calc variable address with index r30 + 1 into YH:YL
	movw	YH:YL,	r23:r22
	add		YL,		r30
	clr		r30
	adc		YH,		r30

	; Load variable value into r31:r30
	ld		r30,	Y+
	ld		r31,	Y

	; Push variable into stack
	push	r31
	push	r30
	rjmp	calc_expression_loop

calc_expression_handle_immediate:
	; Assign r29:r28 to digit r30
	subi	r30,	'0'
	mov		r28,	r30
	clr		r29

calc_expression_handle_immediate_loop:
	; Load char into r30
	ld		r30,	X+

	; Check r30 is digit
	cpi		r30,	'0'
	brlo	calc_expression_handle_immediate_loop_end
	cpi		r30,	'9' + 1
	brsh	calc_expression_handle_immediate_loop_end

	; Transform char r30 into digit
	subi	r30,	'0'

	; Multiply r29:r28 by 10
	ldi		r31,	10
	
	muls	r29,	r31
	mov		r29,	r0

	muls	r28,	r31
	add		r28,	r0
	adc		r29,	r1

	; Add r20 digit to r29:r28
	add		r28,	r30
	clr		r30
	adc		r28,	r30

	rjmp	calc_expression_handle_immediate_loop
calc_expression_handle_immediate_loop_end:
	push	r29
	push	r28
	rjmp	calc_expression_loop

calc_expression_handle_sum:
	; Load b into r29:r28
	pop		r28
	pop		r29

	; Load a into r31:r30
	pop		r30
	pop		r31

	; Add r29:r28 to r31:r30
	add		r30, r28
	adc		r31, r29

	; Save r31:r30
	push	r31
	push	r30
	rjmp	calc_expression_loop

calc_expression_handle_subtract:
	; Load b into r29:r28
	pop		r28
	pop		r29

	; Load a into r31:r30
	pop		r30
	pop		r31

	; Subtract r29:r28 from r31:r30
	sub		r30, r28
	sbc		r31, r29

	; Save r31:r30
	push	r31
	push	r30
	rjmp	calc_expression_loop

calc_expression_handle_multiply:
	clr		r17

	; Load b into r29:r28
	pop		r28
	pop		r29

	; Calc absolute value of b and invert r17 if negative
	tst		r29
	brpl	calc_expression_handle_multiply_b_positive
	
	com		r17
	com		r29
	com		r28
	adiw	r29:r28,1
calc_expression_handle_multiply_b_positive:

	; Load a into r31:r30
	pop		r30
	pop		r31
	
	; Calc absolute value of a and invert r17 if negative
	tst		r31
	brpl	calc_expression_handle_multiply_a_positive
	
	com		r17
	com		r31
	com		r30
	adiw	r31:r30,1
calc_expression_handle_multiply_a_positive:

	; Multiply r29:r28 with r31:r30 into r31:r30
	mul		r31,	r28
	mov		r31,	r0

	mul		r30,	r29
	add		r31,	r0
	
	mul		r28,	r30
	mov		r30,	r0
	add		r31,	r1

	; Invert r31:r30 if r17
	tst		r17
	breq	calc_expression_handle_multiply_result_positive
	
	com		r31
	com		r30
	adiw	r31:r30,1
calc_expression_handle_multiply_result_positive:

	; Save r31:r30
	push	r31
	push	r30
	rjmp	calc_expression_loop

calc_expression_handle_divide:
	clr		r17
	clr		r16

	; Load b into r29:r28
	pop		r28
	pop		r29
	
	; Calc absolute value of b and invert r17 if negative
	tst		r29
	brpl	calc_expression_handle_divide_b_positive
	
	com		r17
	com		r29
	com		r28
	adiw	r29:r28,1
calc_expression_handle_divide_b_positive:

	; Load a into r31:r30
	pop		r30
	pop		r31
	
	; Calc absolute value of a and invert r17 if negative
	tst		r31
	brpl	calc_expression_handle_divide_a_positive
	
	com		r17
	com		r31
	com		r30
	adiw	r31:r30,1
calc_expression_handle_divide_a_positive:

	; Check r29:r28 for zero
	tst		r29
	brne	calc_expression_handle_divide_not_zero
	tst		r28
	brne	calc_expression_handle_divide_not_zero

	; Assign r31:r30 'infinity'
	ser		r30
	ldi		r31,	0x7f
	rjmp	calc_expression_handle_divide_end
calc_expression_handle_divide_not_zero:
	
	; Shift r29:r28 until MSB = 1
calc_expression_handle_divide_shift_loop:
	inc		r16
	lsl		r28
	rol		r29
	brpl	calc_expression_handle_divide_shift_loop

	; Divide r31:r30 by r29:r28
	clr		r24
	clr		r25
	rjmp	calc_expression_handle_divide_loop_end
calc_expression_handle_divide_loop:
	dec		r16
; Divide r29:r28 by 2
	lsr		r29
	ror		r28
; Multiply r25:r24 by 2 and add 1
	sec
	rol		r24
	rol		r25

	; Subtract r29:r28 from r31:r30
	sub		r30,	r28
	sbc		r31,	r29
	
	brpl	calc_expression_handle_divide_loop_end
	; Add r29:r28 from r31:r30 back
	add		r30,	r28
	adc		r31,	r29
	cbr		r24,	0x01
calc_expression_handle_divide_loop_end:
	tst		r16
	brne	calc_expression_handle_divide_loop

calc_expression_handle_divide_end:
	; Invert r31:r30 if r17
	tst		r17
	breq	calc_expression_handle_divide_result_positive
	
	com		r25
	com		r24
	adiw	r25:r24,1
calc_expression_handle_divide_result_positive:
	; Save r25:r24
	push	r25
	push	r24
	rjmp	calc_expression_loop
#include "expression.h"

int16_t calc_expression(const char* expression, const int16_t* variables, int16_t* stack) {
	int16_t op1, op2, temp;
	
	for (const char* it = expression; *it != 0; it++) {
		switch (*it) {
			case '0' ... '9':
				temp = 0;
				while ('0' <= (*it) && (*it) <= '9')
					temp = temp * 10 + (*it++) - '0';
				(*++stack) = temp;
				break;
			case 'x':
				temp = (*++it) - '1';
				(*++stack) = variables[temp];
				break;
			case '+':
				op2 = *stack--;
				op1 = *stack--;
				(*++stack) = op1 + op2;
				break;
			case '-':
				op2 = *stack--;
				op1 = *stack--;
				(*++stack) = op1 - op2;
				break;
			case '*':
				op2 = *stack--;
				op1 = *stack--;
				(*++stack) = op1 * op2;
				break;
			case '/':
				op2 = *stack--;
				op1 = *stack--;
				(*++stack) = op1 / op2;
				break;
			default:
				break;
		}
	}
	return (*stack);
}
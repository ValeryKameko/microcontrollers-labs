#ifndef EXPRESSION_H_
#define EXPRESSION_H_

#include <stdint.h>

int16_t calc_expression(const char* expression, const int16_t* variables, int16_t* stack);

#endif /* EXPRESSION_H_ */
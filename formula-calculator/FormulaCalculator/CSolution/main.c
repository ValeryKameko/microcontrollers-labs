#include <avr/io.h>
#include <avr/eeprom.h>
#include "expression.h"

char EEMEM expression_value[] = "x1 3 - x2 * x3 - x4 3 - /";
uint16_t EEMEM variable_values[] = {10, 2, 4, 5};
uint16_t EEMEM result_value;
enum {
	VARIABLES_COUNT = sizeof(variable_values) / sizeof(*variable_values),
	EXPRESSION_LENGTH = sizeof(expression_value),
	STACK_SIZE = 100
};


int16_t result;
int16_t variables[VARIABLES_COUNT];
char expression[EXPRESSION_LENGTH];
int16_t stack[STACK_SIZE];


int main() {
	result = (int16_t) eeprom_read_word(&result_value);
	eeprom_read_block((int16_t*) variables, variable_values, sizeof(variable_values));
	eeprom_read_block((int16_t*) expression, expression_value, sizeof(expression_value));

	int16_t result = calc_expression(expression, variables, stack);
	
	eeprom_write_word(&result_value, (uint16_t) result);
	
	while (1)
	;
}

